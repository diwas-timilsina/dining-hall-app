//
//  RootTabBarController.swift
//  diningHall
//
//  Created by Diwas  Timilsina on 7/11/15.
//  Copyright (c) 2015 Diwas  Timilsina. All rights reserved.
//

import UIKit

class RootTabBarController: UITabBarController {

    var dayDictionary = ["Mon": 0, "Tue": 1, "Wed": 2, "Thu": 3, "Fri": 4]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var date = NSDate(dateString:"2015-07-15")
        var dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "EEE"
        var dayString = dateFormatter.stringFromDate(date)
        var dayInt = dayDictionary[dayString]
        println(dayInt!)

        if dayInt != nil {
           self.selectedIndex = dayInt!
        }
    }
}
